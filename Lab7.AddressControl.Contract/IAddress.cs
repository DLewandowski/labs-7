﻿using System;
using System.Windows.Controls;

namespace Lab7.AddressControl.Contract
{
    public interface IAddress
    {
        Control Control { get; }

        event EventHandler<AddressChangedArgs> AddressChanged;
    }
}
